import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VecinosPage } from './vecinos.page';

describe('VecinosPage', () => {
  let component: VecinosPage;
  let fixture: ComponentFixture<VecinosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VecinosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VecinosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
