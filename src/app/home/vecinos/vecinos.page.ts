import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-vecinos',
  templateUrl: './vecinos.page.html',
  styleUrls: ['./vecinos.page.scss'],
})
export class VecinosPage implements OnInit {
  titulo:string = "Vecinos"

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location
  ) { }

  navigateToHome(ruta:string)
  {
    this.router.navigate([ruta]);
  }

  goToBack()
  {
    this.location.back();
  }

  ngOnInit() {
  }

}
