import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
titulo: string = "Home"
Table: any
selectedData: any;

constructor(
  public navCtrl: NavController,
  private router: Router,

){ }

navigateToRoute(ruta:string)
{
  this.router.navigate([ruta]);
}

}
