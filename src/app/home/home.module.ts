import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { VecinosPage } from './vecinos/vecinos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      },
      {
        path: 'vecinos',
        component: VecinosPage
      }
    ])
  ],
  declarations: [
    HomePage, 
    VecinosPage
  ],
  entryComponents: [
    VecinosPage
  ]

})
export class HomePageModule {}
